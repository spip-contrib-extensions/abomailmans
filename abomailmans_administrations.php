<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function abomailmans_upgrade($nom_meta_base_version, $version_cible) {

	$maj = [];

	$maj['create'] = [
		['creer_base'],
	];

	$maj['0.30'] = [['maj_tables',['spip_abomailmans']]];
	$maj['0.31'] = [['maj_tables',['spip_abomailmans']]];
	$maj['0.32'] = [['maj_tables',['spip_abomailmans']]];
	$maj['0.33'] = [['maj_tables',['spip_abomailmans']]];
	$maj['0.34'] = [['maj_tables',['spip_abomailmans']]];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function abomailmans_vider_tables($nom_meta_base_version) {
	include_spip('base/abstract_sql');
	sql_drop_table('spip_abomailmans');
	effacer_meta($nom_meta_base_version);
}

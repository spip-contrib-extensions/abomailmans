<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abomailmans?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abomailman' => 'لاذحة بريدية او لائحة نقاش',
	'abomailmans' => 'لوائح بريدية او لوائح نقاش',
	'abonne' => 'اشتراك',
	'activation' => 'تفعيل',
	'active' => 'نشطة',
	'aucune_langue' => 'غير محدد',

	// B
	'bouton_listes_diffusion' => 'اللوائح البريدية',
	'btn_abonnement' => 'اشتراك',
	'btn_desabonnement' => 'الغاء الاشتراك',

	// C
	'champ_titre_label' => 'العنوان',
	'choisir_liste' => 'يجب اختيار لائحة.',
	'contenu_date' => 'المحتوى حسب هذا التاريخ',
	'creation_droits_insuffisants' => 'أذوناتكم غير كافية...',

	// D
	'desactive' => 'متوقفة',
	'destinataire' => 'المرسَل اليه',

	// E
	'email' => 'عنوان البريد',
	'email_abonnement' => 'عنوانكم البريدي',
	'email_envoye' => 'تم ارسال البريد الى اللائحة البريدية: @liste@.',
	'email_oublie' => 'نسيتم إدخال عنوانكم البريدي',
	'emailliste_abomailman' => 'عنوان اللائحة البريدي',
	'emailliste_abosympa' => 'عنوان بريد مدير لائحة Sympa',
	'emailliste_subscribe' => 'اشتراك',
	'emailliste_unsubscribe' => 'الغاء الاشتراك',
	'emails_a_renseigner' => 'العناوين المطلوبة',
	'envoi_apercu' => 'معاينة',
	'envoi_confirmer' => 'التأكيد والارسال',
	'envoi_liste_parametres' => 'قائمة الخصائص (اختياري)',
	'envoi_parametres' => 'الخصائص',
	'envoi_regulier' => 'إرسال آلي',
	'envoi_regulier_info' => 'تركه فارغاً لإيقاف الإرسال الآلي',
	'envoi_regulier_tous_les' => 'إرسال كل',
	'envoi_vers' => 'إرسال الى',
	'envoyer_courier' => 'إرسال بريد',
	'envoyer_courier_liste' => 'إرسال هذا البريد الى هذه اللائحة',
	'envoyer_mailmans' => 'اختيار النموذج ومحتواه',
	'erreur_email_liste_oublie' => 'عنوان اللائحة البريدي إجباري',
	'erreur_nobot' => 'لم يتم التسجيل بسبب خطأ تقني',
	'explication_email_subscribe' => 'عنوان الاشتراك على شكل <code>suffix+subscribe@exemple.org</code>',
	'explication_email_sympa' => 'اذا تم ملء هذا الحقل، يتم اعتبار اللائحة على انها لائحة «Sympa»، والا فلائحة «Mailman» او «ezmlm».',
	'explication_email_unsubscribe' => 'عنوان الغاء الاشتراك',

	// I
	'icone_ajouter_liste' => 'إضافة لائحة جديدة',
	'icone_envoyer_mail_liste' => 'إرسال بريد الى اللوائح من محتوى هذا الموقع',
	'icone_modifier_abomailman' => 'تعديل اللائحة',
	'icone_retour_abomailman' => 'عودة الى اللائحة',
	'info_abomailman_aucun' => 'لا يوجد اي لائحة',
	'info_abomailmans_1' => 'لائحة واحدة',
	'info_abomailmans_nb' => '@nb@  لوائح',
	'info_sisympa' => '[إجباري اذا كانت اللائحة Sympa]',
	'insciption_listes_legende' => 'الاشتراك في اللوائح البريدية',
	'inscription_lettres_legende' => 'الاشتراك في اللوائح البريدية ولوائح النقاش',

	// J
	'je_m_abonne' => 'وضع اشارة لتأكيد الاشتراك او الغاء الاشتراك',

	// L
	'label_etat_liste' => 'وضع اللائحة',
	'label_type_abo' => 'النوع',
	'label_type_ml' => 'لائحة نقاش',
	'label_type_news' => 'لائحة بريدية',
	'langue_liste' => 'لغة اللائحة',
	'legende_inscription_ml' => 'التسجيل في لائحة النقاش',
	'legende_inscription_news' => 'التسجيل في اللائحة البريدية',
	'legende_inscriptions_ml' => 'التسجيل في لوائح النقاش',
	'legende_inscriptions_news' => 'التسجيل في اللوائح البريدية',
	'les_listes_mailmans' => 'لوائح mailmans او sympa او ezmlm المعرّفة',
	'lire_article' => 'قراءة المقال',
	'liste_creee' => 'تم إنشاء اللائحة @id@ (باسم @titre@).',
	'liste_non_existante' => 'اللائحة المطلوبة لا وجود لها او تم الغاؤها',
	'liste_oublie' => 'نسيتم وضع إشارة على لائحة!',
	'liste_supprimee' => 'تم إلغاء اللائحة @id@ (باسم @titre@).',
	'liste_updatee' => 'تم تحديث اللائحة @id@ (باسم @titre@).',

	// M
	'message' => 'مقدمة الرسالة قبل محتوى الموقع',
	'message_confirm_suite' => 'لتأكيد الطلب، الرجاء الرد على طلب التأكيد الذي سيصل الى عنوانكم.',
	'message_confirmation_a' => 'تم ارسال طلب اشتراك في اللوائح التالية:',
	'message_confirmation_d' => 'تم إرسال طلب إلغاء اشتراك في اللوائح التالية:',
	'message_confirmation_unique_a' => 'تم ارسال طلب اشتراك في اللائحة التالية:',
	'message_confirmation_unique_d' => 'تم ارسال طلب الغاء اشتراك في اللائحة التالية:',
	'mot' => 'وعرض مقالات المفتاح',

	// N
	'nom' => 'الاسم والكنية (اختياري)',
	'nouveau_abomailman' => 'لائحة بريدية جديدة',

	// P
	'pas_template_txt' => 'لا توجد نسخة نصية لهذا النموذج',
	'periodicite' => 'يوم.',
	'prenom' => 'الاسم',
	'previsu_html' => 'html',
	'previsu_txt' => 'نص',

	// R
	'rubrique' => 'وعرض مقالات القسم',

	// S
	'souhaite_rester' => 'أرغب في الحصول على جديدكم',
	'sujet' => 'عنوان الرسالة',
	'sujet_obligatoire' => 'عنوان الرسالة إجباري.',
	'suppression_definitive' => 'حذف نهائي!',
	'supprimer' => 'حذف',
	'sympa_message_confirmation' => 'تم ارسال بريد تأكيد الى العنوان:',

	// T
	'template' => 'اختيار النموذج ومحتواه',
	'template_defaut' => 'النموذج الافتراضي',
	'template_defaut_info' => 'اذا كان ملف modele_choisi.txt موجوداً، سيتم إرسال النشرة البريدية بتنسيق html + نص والا فسيتم إرسال تنسيق html وحده.',
	'texte_descriptif' => 'الوصف',
	'titre_abomailman' => 'اسم اللائحة',
	'titre_liste_obligatoire' => 'اسم اللائحة إجباري',
	'toute_liste' => 'كل اللوائح البريدية',

	// V
	'verifier_formulaire' => 'تأكدوا من ملء الاستمارة',
	'veut_s_abonner' => 'يريدون الاشتراك',
	'veut_se_desabonner' => 'يريدون الغاء الاشتراك',
	'voir_modele_depuis' => 'عرض مثل عن النموذج مع',
	'votre_email' => 'عنوان بريدكم'
);
